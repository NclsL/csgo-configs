# csgo-configs


`nvidia-settings -a "DigitalVibrance=1023"`  
values 0 to 1023  

Windows:  
C:\Program Files (x86)\Steam\steamapps\common\Counter-Strike Global Offensive\csgo\cfg\autoexec.cfg  

Linux:  
~/.local/share/Steam/steamapps/common/Counter-Strike Global Offensive/csgo/cfg/autoexec.cfg  

+exec autoexec.cfg -novid -tickrate 128 -nojoy
